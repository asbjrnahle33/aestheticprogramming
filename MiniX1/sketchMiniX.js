
/*
This a program giving you a flower :)

Hope you enjoy
*/





function setup() {
  createCanvas(400, 400);
  background(200);


  // Hand holding a flower (1. frame)
    text('/- _ -\\ '  , 196, 100, 100, 100);
    text('/   /    \\ '  , 193, 110, 100, 100);
    text('/  /       \\'  , 190, 120, 100, 100);
    text('\\   \\      /'  , 190, 130, 100, 100);
    text('\\__\\__/'  , 190, 140, 100, 100);
    text('\\__\\__/'  , 190, 150, 100, 100);
    text('|| ___'  , 206, 160, 100, 100);
    text('__||/__/'  , 192.5, 170, 100, 100);
    text('/__/||'  , 186, 180, 100, 100);
    text(' ___|L._'  , 186, 190, 100, 100);
    text(' -----------./   ._. \\)'  , 150, 200, 100, 100);
    text('  |   / (_\_C\)'  , 180, 210, 100, 100);
    text(' |_   (___)  ' , 180, 220, 100, 100);
    text('  |     (__)  ' , 180, 230, 100, 100);
    text(' |-----´||'   , 180, 240, 100, 100);
    text(' ___|       ||'   , 160, 250, 100, 100);
}


function mousePressed() {
  if (mouseX > 0 && mouseX < width && mouseY > 0 && mouseY < height) {
    const writer = createWriter('flower.txt')
    createCanvas(400, 400);
    background(200);

// Hand without flower (2. frame)
    text(' ___._'  , 186, 190, 100, 100);
    text(' -----------./   ._. \\)'  , 150, 200, 100, 100);
    text('  |   / (_\_C\)'  , 180, 210, 100, 100);
    text(' |_   (___)  ' , 180, 220, 100, 100);
    text('  |     (__)  ' , 180, 230, 100, 100);
    text(' |-----´'   , 180, 240, 100, 100);
    text(' ___|       '   , 160, 250, 100, 100);

// Flower in rewareded (The .txt file w/ 3. frame)
     {
    writer.print('    /-_ -\\' );
    writer.print('   /  /   \\' );
    writer.print('  /  /     \\' );
    writer.print('  \\  \\     /' );
    writer.print('   \\__\\ __/' );
    writer.print('    \\__\\__/' );
    writer.print('      || ___' );
    writer.print('   __ ||/__/' );
    writer.print('  /__/||' );
    writer.print('      ||' );
    writer.print('      ||' );
    writer.print('      ||' );

    }
    writer.close();
  }
}
