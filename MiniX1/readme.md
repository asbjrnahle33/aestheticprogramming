Hello!


Welcome to my first MiniX.


In my RunMe application, i first wanted to make ... something. Therefore i needed som inspiration.
Under the refrence page at p5js, i found a simple, but great code i found a lot of inspiration into.
It was the createWriter() code from this link: https://p5js.org/reference/#/p5/createWriter
When I tried the program i really liked that you got the .txt file downloaded to your computer.


I thought of it as I was givin something to me. Therefore I tested it out first by snatching all of the code
and experimented with it. I tried changing the text and made a simple text saying: "If you click this, you are an idiot". And then if you did so, then you would recive a .txt file saying: "dumbass"


Later on I changed the content to my original idea by reciving something, just like a gift. I wanted to
explore the way you got seomthing 'physical' (or atleast local) to you. Therefore I thought of giving
flowers. I wanted to give flowers to the user.

To do that I needed a picture of a flower and a hand holding it. And I was too lazy to figure out how to
implement a .png or .jpeg file. Therefore I wanted to get a more 'original' way of showing a picture. And
since I only knew how to make text based content. I made the content text based. Therefore it reminded me
of ASCII art. ASCII art is where you take symbols from the computer and make all the symbols line up so they
look like a picture.


I stole then this picture :)
from Luc-Etienne Brachotte
at: https://ascii.co.uk/art/hand


I made some tweaks to the picture, but it was just what I needed.
Therefore I made a program where you recive a flower, hope you enjoy :)

![](MiniX1gif.gif) 

Project ---->   https://asbjrnahle33.gitlab.io/aestheticprogramming/MiniX1/

ReadMe ----> https://gitlab.com/asbjrnahle33/aestheticprogramming/-/blob/main/MiniX1/readme.md

SketchMiniX ----> https://gitlab.com/asbjrnahle33/aestheticprogramming/-/blob/main/MiniX1/sketchMiniX.js




