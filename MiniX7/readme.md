
Hello!
Welcome to my seventh MiniX!

rework of miniX6

Practical: 
Made an update to my MiniX6 where severel things. These include for example: 

- Added a sling for my ball, so it can be launched into the net 
- Added a visual line for the sling, so you can see the amount off force put into the sling with the mouseConstraint 
- Added a picture for basketball and a basketball net. 
- Updated the collision for the net, so it matches with the new net 
- Updated the size of the ball and made some adjustments for the physics in the objects
- Added a reset button (spacebar), so you don not have to refresh the browser every time you wanna make a new shot at the net/goal



Conceptual:

Ive added these elements for it to look more like the product that I wanted in the first place. Im really glad that I could get some more time to finish the project, or atleast 
get a 2.0 version of it lookin a little more clean. I choose my miniX6 cause Ive thought that this was my most ambitious project and I really wanted to finish it. And since 
I was to tired after the creation of the miniX6, then I was pretty happy to get this follow up.

Prior to the sixth miniX, ive made a lot of progress on what ive have learned. These enclude going ut of the way and searching for diffrent kinds of syntaxes in the 2D Matter engine webpage. Also a minor, but a pretty visual step in the progression, using an image for my objects. I have seen other students making objects out of images, and therefore I replicated the same steps as them. 

Reflection on digital culture: 
In my miniX7, I made the program more forth coming and easier to digest. It is also how you on a first notice will experience an easier and more explorative way of thinking, when running the the runMe. The new revisit to my prior miniX6, is far better cause of its "throwing you in the spot" kind of way. And also in the prior case you would take a moment to think what is actually going on, because of all the boxes. Some that can move and some that can't. Therefore not making an objetive from the get go, because the lack of information. 

When running the program, you are set into a space of a basketball arena, therefore you instantly make a connection to the rules and basics of basketball, without even explaining it. When seeing the field, you are also intrigued to "play" with the ball, since it will start bouncing on the sling it its set to. Therefore making an explorative narrative in the program. And since you have a goal, (literally) then you will naturllay take a shot at the goal with all the prior context at hand. 

I see alot of explorative aspect in miniX, in a asthetic programing setting. 



Project ---->  https://asbjrnahle33.gitlab.io/aestheticprogramming/MiniX7/

![](basket.gif)

SketchMiniX ----> https://gitlab.com/asbjrnahle33/aestheticprogramming/-/blob/main/MiniX7/sketchMiniX7.js

ReadMe ----> https://gitlab.com/asbjrnahle33/aestheticprogramming/-/blob/main/MiniX7/readme.md


Credit:

Engine: https://brm.io/matter-js/

Coding train: https://www.youtube.com/watch?v=TDQzoe9nslY&t=951s
