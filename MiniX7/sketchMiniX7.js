
/*

Side notes:

for(let i = 0; i < 5; i++) {


  players[i] = new Player("brown", 50+100*i, 100, 50);

  players[i].placePlayer();

const { Engine, World, Bodies, MouseConstraint, Mouse, Constraint} = Matter;

  End of side notes

}
*/

const { Engine, World, Bodies, MouseConstraint, Mouse, Constraint} = Matter;


let ground;
let puzzle;
let box;
let piece;
let world,engine;
let mouseConstraint;
let slingshot;


function preload() {
  basketball = loadImage('assets/basketball.png');
  net = loadImage('assets/Net.png');
}
//19.21

 /*
 Sets up the:
  A double syntax where I set up the Canvas with a height and a width,
  meanwilhe I make a constant called "canvas" so I can use it later to show
  the engine what it is. By doing so, then the engine can see in which
  height and width in the canvas it can use the mouseConstraint I make later.

  The engine is created, and the world is created aswell, when you create
  the engine

  Grabs engine from the world. So now you have the Matter- engine and world.

  A new object from the class Ground is being made, with the name "ground, top, right and left"

  A new object from the class Box is being made, with the name "box"

  A new object from the class Bird is being made, with the name "bird"

 */
function setup() {
  const canvas = createCanvas(600,400);
  engine = Matter.Engine.create();
  world = engine.world;

//walls for the objects
  ground = new Ground(width/2, height-10, width, 20);
      top = new Ground(width/2, height-400, width, 1);
      right = new Ground(width, height/2, 5, height);
        left = new Ground(width-600, height/2, 5, height);

//collider for basket hoof
       puzzle = new Ground(width/2+210, height/2-25, 10, 300);
        puzzle = new Ground(width/2+150, height/2-50, 10, 30);
        puzzle = new Ground(width/2+210, height/2-50, 10, 70);



//the two objects with physics
  box = new Box(450 , 300, 25 ,75)
  piece = new Piece(50, 300, 20);

//sling shot for the basketball
  slingshot = new Shot(125, 250, piece.body);




  const mouse = Mouse.create(canvas.elt);
  const options = {
    mouse: mouse
  };

  // A fix for HiDPI displays
  mouse.pixelRatio = pixelDensity();
  mConstraint = MouseConstraint.create(engine, options);
  World.add(world, mConstraint);
}

function mouseReleased() {
  setTimeout(() => {
    slingshot.fly();
  }, 100);
}

function keyPressed() {
  if (key == ' ') {
    World.remove(world, piece.body);
    piece = new Piece(150, 300, 20);
    slingshot.attach(piece.body);
  }
}

/* Loops:
  background
  the engine updating, so the game always has physics.
  the class: ground
  the class: box
  the class: bird
*/
function draw(){
  background(0);
  Matter.Engine.update(engine);
  ground.show();
  box.show();
  piece.show();
  slingshot.show();


//Basket hoof
/*
 fill(170,70,0);
  rect(510, 100, 10, 50);
  rect(420, 100, 10, 50);
    rect(420, 150, 100, 10);

      rect(420, 150, 100, 10);
      */

image(net, 330, 0)




}
