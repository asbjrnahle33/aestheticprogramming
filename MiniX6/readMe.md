Hello!

Welcome to my sixth MiniX!

Practical:

In my MiniX i stared off by trying something completely new. Implementing a 2D engine into the p5.js platform. I really wanted to challenge myself, and I certainly did. 

I figured since we are programming with objects in mind, why not have an engine, so I don't have to make so many calculations for making "fake physics" into the game and since every object are going to interact with eachother, then why not have a precalculated engine doing this for me. 

Well it is easier said than done. I used the 2D engine for JavaScript called Matter. I had to implement it through their website in the index and 
on their website you can check out diffrent and new syntaxes based on the engine. 

For example: 
Matter.MouseConstraint.create(engine);
Matter.Engine.update(engine);
Matter.Engine.create();

These three syntaxes I had to look up and see what they exactly did. 

This MiniX is properly the MiniX Ive learned the most from, the amount of syntaxes and challenges Ive made for myself really payed off in the end. Even though this might not be my most conceptually piece of code, Ive think I really improved in the technical aspect of things. Ive also seen a lot of the coding train, and have taken a lot of inspiration from some of his work with engines in p5.js 

Some examples Ive learned:
Engines 
Classes
Consrtuctor 
Extends (extenstion of classes: sub-classes)


Conceptual:
On the conceptual side, I have been reflecting on how objects in games are. I have already had this thought in the past, but it is a nice reminder on how games work. I think that I already think pretty technical, so therefore Ive found it rather intresting to pin out the diffrent kinds of objects in for example video games. How the diffrent players, enemies, equipment, projectiles and world objects can interact with eachother. 

A great example to the example is: 

A first person shooter video game. You, a player, controls a player figure, a object. The player figure, interacts with a gun, a object. The gun interacts with projectile when fired, a object. The bullet, interacts with another player or enemy, that also is a object. 




Project ----> https://asbjrnahle33.gitlab.io/aestheticprogramming/MiniX6/

![](gig.gif)

SketchMiniX ----> https://gitlab.com/asbjrnahle33/aestheticprogramming/-/blob/main/MiniX6/sketchMiniX5.js

ReadMe ----> https://gitlab.com/asbjrnahle33/aestheticprogramming/-/blob/main/MiniX6/readme.md


Credit: 

Engine: https://brm.io/matter-js/ 
Coding train: https://www.youtube.com/watch?v=TDQzoe9nslY&t=951s
