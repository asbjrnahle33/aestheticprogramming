Hello! 

Welcome to my second MiniX! 

In this miniX we where givin an obejtive in having atleast two emojies. Therefore i focused on the feeling and how you got 
to set emotions. I took some inspiration from one of my classmates. Albin made a DVD icon that flew around and it checked if it reached 
the end of the canvas, when it did that, it then executed a command. By seeing this I wanted to do something with trackers of the 
mouseX and mouseY axis. 

I then made a tracker that executed a command, based on if the mouse cursor was in the frame or not. After that I thougt 
of the attention people on socal media want. Therefore I captured the essences of the emotions you actually make if you do, or do not get
the attention from social media. For example if you don't get the amount of likes or hearts on your instagram or facebook. 

But as soon you give some attention, the emoji turns to a heavnly angel. So my topic is about: Attention, toxicity and 'double-faced' persons.

I used some random .gifs and .png files on the internet, and I stole .gif from the intro of the Anime series Deathnote to make the background
of the angry expression. 

Project ----> https://asbjrnahle33.gitlab.io/aestheticprogramming/MiniX2/

  ![](MiniX2/ezgif.com-gif-maker2.gif)

SketchMiniX ----> https://gitlab.com/asbjrnahle33/aestheticprogramming/-/blob/main/MiniX2/sketchMiniX2.js

ReadMe ----> https://gitlab.com/asbjrnahle33/aestheticprogramming/-/blob/main/MiniX2/readme.md
