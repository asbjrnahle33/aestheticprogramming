let xoff = 500;
let xincrement = 0.01;

let goal2;
let goal1 = 150;

let score1 = 0;
let score2 = 0;

function setup() {
  createCanvas(710, 400);
  background(0);
  frameRate(500);
  noStroke();

  goal2 = width- 175

}

function draw() {
  // Create an alpha blended background
  background(0);

  //let n = random(0,width);  // Try this line instead of noise


  let n = floor(noise(xoff) * 710);


  // With each cycle, increment xoff
  xoff += xincrement;

  // Draw the ellipse at the value produced by perlin noise
  fill(200);
  ellipse(n,  height / 2, 32, 32);

  rectMode(CENTER);

  rect(goal1, height/ 2 ,25,100)

  rect(goal2 , height/ 2 ,25,100)

  if(goal1 == n) {
    score1++;
  }
  if(goal2 == n) {
    score2++;
  }


print(n, goal1)
  textSize(50)
  text(score1, 85, 80);
  text(score2, 570, 80);

}
