Hello!
Welcome to my fifth MiniX!


Practical:

In my MiniX i stared off  by finding some automation so I could  later apply rules to it, and I found perlin noise  to be very intresting cause of it's sudo-randomness. I really like the concept of random, but visually I rather wanted something that was a bit less too random, therefore perlin noise was a great option for me. I started off by making the initial code for a ball to move around "randomly" on the x-axes after that I thought of making a soccer field. It also was a great idea cause of I still had to make some rules for the automation, but since I had my idea with soccer then I already have some rules to go after. Therefore I made some goals out of rectangels, pretty simple, but enough for me. Then I made a counter to keep track of how many times the ball have scored in the goal and a tracker, that track when the ball is in the goal. 
I did have som issues cuase of checking of the ball, since the way of my tracking. The tracking is done by checking exactly if the ball is on the same x coordinate as the goal. But to do so I had to make the noise set to a floor, then I could get the same number, instead of a decimal number. The bad thing is that the tracker only checks for that surtain point, so you will see that even if the ball has gone into the goal, sometimes it wont count as a score cause maybe it went to far, or didn't  hit the exat number where it counts up. It would be far easier if it was a collider instead, and seen as an  game object in stead of a circle with semi-randomly x coordination. So I could diffeently make this better, but this week was pretty rough :))


Conceptual:

When I made my game I thought of how it actually wasn't one, even though it has the same rules as a normal game of soccer or football, it did it by itself. The game-like scenario has been taken away cause of your actions have been taken away. There is no input for the person to do, only spectating. It corralates pretty well with rhe industry nowadays. Automation is taking over the world and it is doing it really good. In fact so good that people start loosing there jobs over it. 

I think that this topic of automation when we started wasn't that big of a topic, but I learned a lot, when I actually made my own automation in p5.js. It struck to me, in a certain kind of way that I really didn't except how much it inpacted on me. But that is only my oppinion on it. Diffently one of the most intresting topics, but no so much in the pratical department for me. 




Project ----> https://asbjrnahle33.gitlab.io/aestheticprogramming/MiniX5/

![](gif.gif)

SketchMiniX ----> https://gitlab.com/asbjrnahle33/aestheticprogramming/-/blob/main/MiniX5/sketchMiniX5.js

ReadMe ----> https://gitlab.com/asbjrnahle33/aestheticprogramming/-/blob/main/MiniX5/readme.md
