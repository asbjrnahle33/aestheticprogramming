
Hello!

Welcome to my third MiniX!

Practical:

In my MiniX i made a time wasting machine. There is a text with a quote on the top of the program, a Rectangle bar with another text, and one where it shows you 
how many seconds has passed since you started the program. To start the program  you smply have to push down your mouse button 1 and hold it down. Then the rectangle will dissapear and 
you will be givin a array of text at your mouse cursor "looping" through five diffrent words telling that you are wasting your time. At the samme time the seconds wasted variable will increase. 
While this is happening, a small circle appears after sometime in the middle. The circle represents how many secnods that their have passed and will grow continuously. This will all reset, if you do not hold the mouse button down. 

Also I have no for loops in my code, but that was because of the purpose of the loop. I do not need it to repeatedly go through my loop so fast, that you would not be able to see how much time you have wasted, cause of the loop going so fast. Therefore I made my own loops that fitted to the setting with seconds so you would be able to see the time pass, and be aware about it. 


Conceptual:

the  idea came from a quote that I heard in some youtube video I browsed through some time ago. I don't really remeber which video, but the qoute kinda stuck to me for some reason. It was "Wasting my time, is diffrent than me wasting my time" 
I think it was both entertaining and had some other value to it. Therefore I made my main concept of my MiniX to waste time, but by doing it yourself counsiously. My thinking is the  idea of a throbber is the same as knowingly in partecepating in waiting, therefore I made my waiting simulater. And instead of looping "all the time" I made it so you loop it yourself. It makes it you becoming the loop, instead of the program. In the meantime im setting up a "game-like" setting. Where the loop becomes a game where you are challenging yourself in how long you can hold the button down.  

There is also this fake goal, where you start out with seeing nothing, just the timer going up, but then you realize that there is a circle slowly apearing. And when it pops up you feel rewared in some sort cause you have sacrificed some of your time to make it apear and grow. And cause of this you you get a another feeling of expectation if you wait even further. Maybe, just maybe, will there happen something else when the circle gets bigger. Maybe if you sacrifice enough time there will happen an suprise like before and your time would not have been for nothing. But no, sadly the circle only gets bigger over time, and in the end you would not even be able to see whats on the screen cause of the surface of the circle. 



















Project ----> https://asbjrnahle33.gitlab.io/aestheticprogramming/MiniX3/

 ![](ezgif.com-gif-maker.gif)

SketchMiniX ----> https://gitlab.com/asbjrnahle33/aestheticprogramming/-/blob/main/MiniX3/sketchMiniX3.js

ReadMe ----> https://gitlab.com/asbjrnahle33/aestheticprogramming/-/blob/main/MiniX3/readme.md
